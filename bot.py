import logging
import os

import dotenv
from telegram import Update
from telegram.ext import ApplicationBuilder
from telegram.ext import CommandHandler
from telegram.ext import ContextTypes

dotenv.load_dotenv()


TELEGRAM_API_TOKEN = os.getenv("TELEGRAM_API_TOKEN")
WEBHOOK_URL = os.getenv("WEBHOOK_URL")
PORT = int(os.getenv("PORT", "8443"))
ENV = os.getenv("ENV")


async def hello(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    update.message.reply_text("Hello, world! 🤖")


def start_bot():
    application = ApplicationBuilder().token(TELEGRAM_API_TOKEN).build()
    application.add_handler(CommandHandler("hello", hello))

    if ENV:
        application.run_webhook(
            listen="0.0.0.0",
            port=PORT,
            url_path=TELEGRAM_API_TOKEN,
            webhook_url=f"{WEBHOOK_URL}/{TELEGRAM_API_TOKEN}",
        )
    else:
        logging.info("Starting in local polling mdoe")
        application.run_polling()


def main() -> int:
    logging.basicConfig(
        level=logging.INFO if ENV else logging.DEBUG,
        style="{",
        format="{name} -- [{levelname}] {asctime}: {message}",
    )

    start_bot()

    return 0


if __name__ == "__main__":
    raise SystemExit(main())
