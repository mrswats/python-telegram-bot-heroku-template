# Telegram Bot Template

This is a repo to use as a template to create telegram bots. This idea comes
from a talk I gave to my colleagues at [Hybrid Theory](https://hybridtheory.com/).

## Requirements:

- Python 3.10+
- Git

## Installing Local dependencies

With python poetry to install dependenies it is as easy as:

```console
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```
## Creating the bot

To create the bot and obtain the API key you first need to talk to [The Bot
Father](https://t.me/thebotfather).

## Environment variables

THis template uses python-dotenv to loead environment variables easily. Copy
the dot-env.example file into `.env` and change the variables as needed. Per
tal de poder executar en local el bot, es necessari l'ús d'un fixter `.env`
dins el mateix directori. Aquest fitxer ha de contenir el token que et proveeix
el Father Bot:

## Running the bot locally

Once the environment variables are set up, just run

```console
python ./bot.py
